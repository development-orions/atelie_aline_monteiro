<?php

use Illuminate\Database\Seeder;

class FragmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fragments')->insert([
            'name' => Str::random(10),
            'product_id' => 1,
        ]);
    }
}
