@extends('layouts.admin.app')

@section('style')
    <link rel="stylesheet" href="{{ asset('/css/admin/kits/index.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css">
@endsection


@section('content')


    @if ($kits)

        <div class="row px-2 px-md-0">
            <div class="col-12 col-md-10 offset-md-1 mt-4">

                @include('admin.kits.modal_addKit')

                @include('admin.release.kit')

            </div>
            <div class="col-12 col-md-10 offset-md-1 mt-3" id="message">
                @include('flash::message')
            </div>
        </div>

        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-12 col-md-10 px-2">
                <table class="rwd-table">
                    <tr>
                        <th>ID</th>
                        <th>Nome do produto</th>
                        <th>Criado em</th>
                        <th>Status</th>
                        <th class="text-center">Ações</th>
                    </tr>

                    @foreach ($kits as $kit)
                        <tr>

                            <td data-th="ID">{{ $kit->id }}</td>
                            <td data-th="Nome do Produto">{{ $kit->name }}</td>
                            <td data-th="Criado em">{{ $kit->created_at }}</td>
                            <td data-th="Status">ATIVO</td>
                            <td data-th="Ações" class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('admin.kits.edit', ['kit' => $kit->id]) }}"
                                        class="btn btn-primary">VER KIT</a>
                                    <form action="{{ route('admin.kits.destroy', ['kit' => $kit->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">DELETAR</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 mb-4">
                {{ $kits->links() }}
            </div>
        </div>
    @endif

@endsection


@section('script')
    <script>
        let baseUrl = '{{ url('/') }}';
        const getInformations = () => {
            let radios = document.getElementsByName('kit');
            let kit = null;
            radios.forEach(radio => {
                if (radio.checked) {
                    kit = radio.value;
                }
            });

            let baseUrl = '{{ url('/') }}';
            

            all_ajax({
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                'method': 'POST',
                "url": `${baseUrl}/admin/release/kits`,
                'payload': {
                    'kit': kit
                },
                'callback': showMessage,
                'args': undefined
            });

        }

        const showMessage = (data) => {
            let div = document.getElementById('message');
            div.innerHTML = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                    ${data.message} <span id='link'>${baseUrl}/client/${data.code}</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>`;

                    closeModal('releaseKit', 'kit');
        }

    </script>
@endsection
