require('./bootstrap');




window.sweetAlert_confirm = (title, text, showDenyButton, showCancelButton, confirmButtonText, confirmButtonColor, denyButtonText, denyButtonColor, callback, args) => {
    Swal.fire({
        title: 'Tem certeza que deseja excluir esta imagem?',
        text: 'Assim que excluir, não será possível recuperá-la.',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: `SIM`,
        confirmButtonColor: `#a20a0a`,
        denyButtonText: `NÃO`,
        denyButtonColor: '#556052'
    }).then((result) => {
        if (result.isConfirmed) {
            callback(args);
        }
    });
}


window.all_ajax = (args) => {
    $.ajaxSetup({
        headers: args.headers
    });

    $.ajax({
        type: args.method,
        url: args.url,
        data: args.payload,
        success: function(data) {
            if (args.callback !== undefined) {
                if (args.args !== undefined) {
                    args.callback(args.args);
                } else {
                    args.callback(data);
                }
            }
        },
        error: function(data) {
            alert('error');
        }
    });
}




window.closeModal = (id, nameUnchecked) => {
    $(`#${id}`).modal('hide');
    document.querySelector('.modal-backdrop').remove();
    unchecked(nameUnchecked);
}

window.unchecked = (name) => {
    let checks = document.getElementsByName(`${name}`);
    checks.forEach(check => {
        check.checked = false;
    });
}

window.copyLink = () => {
    var text = document.getElementById("link").innerHTML;
    try {
        navigator.clipboard.writeText(text);
        showToast('success', 'Link copiado com sucesso, envie ao cliente.');
    } catch (error) {
        showToast('danger', 'OPS! Houve um erro ao copiar o texto.');
    }
}

window.showToast = (type, message) => {
    if (type == 'success') {
        toastr.success(`${message}`);
    } else {
        toastr.error(`${message}`);
    }
}





