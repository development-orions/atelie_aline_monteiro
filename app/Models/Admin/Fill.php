<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Fill extends Model
{
    protected $fillable = ['name', 'location', 'fragment_id', 'created_at', 'updated_at'];

    public function fragment()
    {
        return $this->belongsTo(Fragment::class);
    }
}
