@extends('layouts.client.app')


@section('style')

<link rel="stylesheet" href="{{ asset('css/client/custom.css') }}">

@endsection

@section('content')

<svg class="position-absolute" style="z-index: -5">
    <defs>
        @foreach ($products as $product)

        @foreach ($product->fragments as $fragment)

        @foreach ($fragment->fills as $fill)

        @if ($fill->status == 0)
        <pattern id="pattern-{{ $fill->id }}" patternUnits="userSpaceOnUse" width="400" height="400">
            <image src="{{ asset('/storage/' . $fill->location) }}" href="{{ asset('/storage/' . $fill->location) }}"
                x="0" y="0" width="400" height="400" />
        </pattern>
        @endif
        @endforeach

        @endforeach

        @endforeach
    </defs>
</svg>




<!-- MultiStep Form -->
<div class="row m-0" id="multistep">
    <div class="col-12 col-md-10 offset-md-1">
        <!-- start of form -->
        <form id="msform" method="POST" action="{{ route('client.custom.effect') }}">

            <div class="row m-0 bg-white"
                style="width: 80%; margin-left: 10% !important; box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);">
                <div class="col-12 px-3 pt-4">
                    <div class="message">

                    </div>

                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <strong>
                            Seja muito bem-<span id="welcome"></span>,
                            <span id="parentName"></span>!
                        </strong>
                        <div>
                            <span>
                                Estamos felizes em ter você aqui na nossa plataforma 🎉🎉🎉
                            </span>
                        </div>
                        <div>
                            <span>
                                Caso precise de ajuda com a personalização dos itens, você pode <strong>assistir este <a
                                        href="#" onclick="event.preventDefault(); showModalvideo()">vídeo</a></strong>
                            </span>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>
                            Observações:
                        </strong>

                        Todos os produtos são
                        <strong>meramente ilustrativos
                        </strong>
                        e os tons das
                        <strong>estampas podem variar</strong> levante. ✅
                    </div>


                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Olá,
                            <span id="parentGender"></span>!
                        </strong>
                        Agora você pode personalizar os itens do seu bebê 🥰💕

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>

            @csrf
            <input type="text" name="code" value="{{ $code }}" class="d-none">
            <!-- start of progressbar -->
            <ul id="progressbar" class="" style="box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);">
                @foreach ($products as $product)
                <li @if ($loop->first) class="active"@endif>
                    {{ $product->name }}
                </li>
                @endforeach
            </ul>
            <!-- end of progressbar -->

            @foreach ($products as $product)
            <!-- start of fieldset -->
            <fieldset class="mb-5" id="test">
                <input type="text" class="d-none" name="product-{{ $product->id }}" value="{{ $product->name }}">
                <div class="col-md-12 pt-4">


                    <h2>{{ $product->name }}</h2>

                    <div class="row mt-4">
                        <div class="col-12">
                            @foreach ($product->fragments as $fragment)
                            <input type="text" class="d-none input-filled" id="fragment-{{ $fragment->id }}"
                                name="product-{{ $product->id }}[]" value="">
                            <h5>{{ $fragment->name }}</h5>
                            @foreach ($fragment->fills as $fill)
                            @if ($fill->status == 0)
                            <button class="btn" onclick="
                            event.preventDefault(); 
                            setFillOnFragment('{{ $fragment->name }}','{{ $fill->id }}', '{{ $product->name }}');
                            setFillOnInput('{{ $product->name }}', '{{ $fragment->id }}', '{{ $fill->id }}');
                            ">
                                <img src="{{ asset('/storage\/') . $fill->location }}" height="50px" width="50px">
                            </button>
                            @endif
                            @endforeach
                            @endforeach
                        </div>
                    </div>

                    <div id="svgsCarousel-{{ $product->id }}" class="carousel slide text-center mt-5"
                        data-ride="carousel">

                        <div class="carousel-inner">
                            @foreach ($product->svgs as $svg)
                            <div class="carousel-item @if($loop->first) active @endif">
                                {!! file_get_contents(asset($svg->location)) !!}
                            </div>
                            @endforeach
                        </div>

                        <a class="carousel-control-prev" href="#svgsCarousel-{{ $product->id }}" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#svgsCarousel-{{ $product->id }}" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div>


                <!-- start of footer -->
                <div class="col-md-12">
                    <hr>
                </div>


                @if ($loop->first)
                @if ($loop->count !== 1)
                <input type="button" name="next" class="next action-button begin-verification" value="Próximo" />
                @endif
                @endif

                @if ($loop->last)

                @if ($loop->count !== 1)
                <input type="button" name="previous" class="previous action-button-previous" value="Anterior" />
                @endif

                <!--<input type="submit" name="submit" class="submit action-button" value="Finalizar" onclick="submitForm()" />-->
                <button class="submit action-button" onclick="event.preventDefault(); validation();">Concluir</button>
                @endif




                @if (!$loop->first && !$loop->last)
                <input type="button" name="previous" class="previous action-button-previous" value="Anterior" />
                @if ($loop->count !== 1)
                <input type="button" name="next" class="next action-button begin-verification" value="Próximo" />
                @endif
                @endif

                <!-- end of footer -->
            </fieldset>
            <!-- end of fieldset -->
            @endforeach




        </form>
        <!-- end of form -->
    </div>
</div>
<!-- /.MultiStep Form -->


<!-- Button trigger modal -->
<button type="button" class="d-none" id="triggerModalVideo" data-toggle="modal" data-target="#modalVideo">
    Trigger Modal Video
</button>

<!-- Modal -->
<div class="modal fade" id="modalVideo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Aprendendo a personalizar itens na plataforma 😜</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <video controls="true" src="{{ asset('video/teste.mp4') }}" width="100%"></video>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Já assisti o vídeo</button>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="{{ asset('js/client/custom.js') }}"></script>
@endsection