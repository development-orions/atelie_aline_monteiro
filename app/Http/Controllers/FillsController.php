<?php

namespace App\Http\Controllers;

use App\Http\Requests\FillRequest;
use App\Repositories\FillRepository;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class FillsController extends Controller
{

    public function __construct(FillRepository $fillRepository)
    {
        $this->fillRepository = $fillRepository;
    }

    public function store(FillRequest $fillRequest)
    {
        $input = $fillRequest->all();


        if (!$input['fill-name']) {
            flash('O campo nome é obrigatório na criação da estampa')->warning()->important();
        } else if (!$fillRequest->hasFile('files')) {
            flash('É obrigatório carregar uma foto')->warning()->important();
        } else {
            if ($fillRequest->hasFile('files')) {
                foreach ($fillRequest->file('files') as $f) {
                    $location = $f->store('fills', 'public');
                    $this->fillRepository->create([
                        'name' => $input['fill-name'],
                        'location' => $location,
                        'fragment_id' => $input['fragment_id'],
                        'updated_at' => null
                    ]);
                }
            }
        }
        flash('<strong>Cadastro efetuado com sucesso!</strong> Agora seus clientes já podem personalizar o produto com a estampa.')->success()->important();
        return redirect()->route('admin.products.edit', ['product' => $input['product-id']]);
    }


    public function destroy($fill)
    {
        $fill = $this->fillRepository->find($fill)->delete();
    }


    public function update($id)
    {
        $fill = $this->fillRepository->find($id);

        $fill->status = $fill->status == 1 ? false : true;
        $fill->save();
        
        return 'fill was updated successfully';
    }
}
