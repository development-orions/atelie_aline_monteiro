<?php

namespace App\Repositories;

use App\Models\Admin\Fill;
use App\Repositories\BaseRepository;

/**
 * Class FillRepository
 * @package App\Repositories
 * @version November 10, 2020, 2:14 pm UTC
*/

class FillRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'location',
        'fragment_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Fill::class;
    }
}
