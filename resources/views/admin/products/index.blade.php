@extends('layouts.admin.app')

@section('style')
<link rel="stylesheet" href="{{ asset('/css/admin/products/index.css') }}">
@endsection


@section('content')


@if ($products)

<div class="row">
    <div class="col-11 offset-1 col-md-10 offset-md-1 mt-4">

        <button class="btn btn-success btn-radius" disabled>Adicionar</button>

        <!-- include release -->
        @include('admin.release.products')

    </div>
    <div class="col-12 col-md-10 offset-md-1 mt-3" id="message">
        @include('flash::message')
    </div>
</div>

<div class="row d-flex align-items-center justify-content-center">
    <div class="col-12 col-md-10 overflow-hidden">
        <table class="rwd-table">
            <tr>
                <th>ID</th>
                <th>Nome do produto</th>
                <th>Descrição</th>
                <th>Criado em</th>
                <th>Ações</th>
            </tr>

            @foreach ($products as $product)
            <tr>

                <td data-th="ID">{{ $product->id }}</td>
                <td data-th="Nome do Produto">{{ $product->name }}</td>
                <td data-th="Descrição" class="th-description">{{ $product->description }}</td>
                <td data-th="Criado em">{{ $product->created_at }}</td>
                <td data-th="Ações">
                    <a href="{{ route('admin.products.edit', ['product' => $product->id]) }}"
                        class="btn btn-primary">EDITAR</a>
                    <button class="btn btn-danger" disabled>DELETAR</button>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>



@endif
@endsection


@section('script')

<script>

    let baseUrl = '{{ url('/') }}';

   window.getInformations = () => {
    let checks = document.getElementsByName('products[]');
    let products = [];
    checks.forEach(check => {
        if (check.checked) {
            products.push(check.value)
        }
    });
    all_ajax({
        'headers': {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        'method': 'POST',
        'url': `${baseUrl}/admin/release/products`,
        'payload': {
            'products': products
        },
        'callback': responseMessage,
        'args': undefined
    });
}


window.responseMessage = (data) => {
    const divMessage = document.getElementById('message');
    if (data.status === 200) {
        divMessage.innerHTML = `<div class="alert alert-success" role="alert" id='messageSuccess'>
                            <h4 class="alert-heading">Sucesso em liberar lista!</h4>
                            <p>Você liberou a lista com os produtos, agora você pode copiar o link e enviar paara seu cliente.</p>
                            <hr>
                            <p class="mb-0">
                                <a href="#" class="badge badge-dark p-2" onclick='copyLink()'>Copiar</a>
                                &nbsp;<span id='link'>${baseUrl}/client/${data.code}</span> 🚀
                            </p>
                            </div>`;
    } else {
        divMessage.innerHTML = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${data.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>`;
    }

    closeModal('releaseProduct', 'products[]');

}

</script>

@endsection