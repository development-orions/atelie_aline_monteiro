<?php

namespace App\Repositories;

use App\Models\Admin\Kit;
use App\Repositories\BaseRepository;

/**
 * Class KitRepository
 * @package App\Repositories
 * @version November 12, 2020, 5:40 pm UTC
*/

class KitRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Kit::class;
    }
}
