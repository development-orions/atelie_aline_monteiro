<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fills', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 100);
            $table->text('location', 1500);
            $table->unsignedBigInteger('fragment_id');

            $table->timestamps();
            $table->foreign('fragment_id')->references('id')->on('fragments');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fills');
    }
}
