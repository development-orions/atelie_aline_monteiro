<?php

namespace App\Http\Controllers;

use App\TermsAndConditions;
use App\User;
use Illuminate\Http\Request;

class ConfigController extends Controller
{

    public function __construct(User $user, TermsAndConditions $termsAndConditions)
    {
        $this->user = $user;
        $this->terms = $termsAndConditions;
    }

    public function index()
    {
        $user = $this->user->find(auth()->user()->id);
        $termsAndConditions = $this->terms->find(1);
        return view('admin.configs.index', ['profile' => $user, 'termsAndConditions' => $termsAndConditions]);
    }


    public function updateTermsAndConditions(Request $request, $id)
    {
        $data = $request->all();
        
        $termsAndConditions = $this->terms->find($id);
        $termsAndConditions->body = $data['body'];
        $result = $termsAndConditions->save();

        return $result ? 'terms_ok':'terms_fail';
    }

}
