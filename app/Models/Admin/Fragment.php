<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Fragment extends Model
{
    protected $fillable = ['name', 'product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function fills()
    {
        return $this->hasMany(Fill::class);
    }
}
