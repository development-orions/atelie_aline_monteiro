@extends('layouts.admin.app')

@section('style')
<link rel="stylesheet" href="{{ asset('/css/admin/products/edit.css') }}">
@endsection


@section('content')




<div class="row">



    <div class="col-12 col-md-6 offset-md-3 px-2 px-md-0">



        <form method="POST" id="form-edit" action="{{ route('admin.products.update', ['product' => $product->id]) }}">
            @csrf
            @method('PUT')



            <h2 class="my-4" id="main-title">Editando Produto</h2>
            <div id="message-success"></div>

            @include('flash::message')

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="name">Nome do produto</label>
                    <input disabled type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name"
                        value="{{ $product->name }}">
                    @error('name')
                    <small class="invalid-feedback">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label for="description">Descrição</label>
                <textarea name="description" id="description" cols="30" rows="3"
                    class="form-control">{{ $product->description }}</textarea>
            </div>




            <!--
                                *
                                *   fragments registered in a given product
                                *
                                -->
            <h2 class="mt-4">Fragmentos</h2>
            <div class="row">
                @foreach ($fragments as $fragment)
                <div class="col-12 col-md-4 p-1">
                    <div class="card-item">
                        <h5>{{ $fragment->name }}</h5>

                        <a class="btn btn-primary" data-toggle="modal"
                            data-target=".modal-fill-{{ $fragment->id }}">Estampas</a>

                        <!--
                                                *
                                                *   modal to display registered fills
                                                *
                                                -->
                        @include('admin.fills.modal')

                    </div>
                </div>
                @endforeach
            </div>
        </form>

        <!--
                            *
                            *   modal to create register fills
                            *
                            -->
        @include('admin.fills.modal-new')

        <h2 class="mt-4">Imagens para personalizações</h2>

        <div class="row">
            @foreach ($svgs as $svg)
            <div class="col-12 col-md-4 p-1" data-id="{{ $svg->id }}">
                <i class="fa fa-times-circle btn-remove" onclick="removeApresentationPhoto(this)"></i>
                <img src="{{ asset('') . $svg->location }}" height="200px" width="100%">
            </div>
            @endforeach
        </div>

        <div class="pl-1 py-4">
            <button class="btn btn-success"
                onclick="event.preventDefault(); document.getElementById('form-edit').submit()">Salvar
                Alterações</button>
        </div>

    </div>

</div>




@endsection


@section('script')
<script src="{{ asset('js/admin/products/edit.js') }}"></script>

<script>
    function handleStatus (elm, id, status) {
        let url = `{{ url('/') }}`;
        
        if (status == 1) {
            elm.className = 'fa fa-bookmark btn-disable';
            elm.setAttribute('onclick', `handleStatus(this, ${id}, 0)`);
        } else {
            elm.className = 'fa fa-bookmark-o btn-enable';
            elm.setAttribute('onclick', `handleStatus(this, ${id}, 1)`);
        }

        

        all_ajax({
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            'method': 'GET',
            "url": `${url}/admin/fills/${id}/update`,
            'payload': undefined,
            'callback': showMessage,
            'args': undefined
        });
    }

    function showMessage(message){
        showToast('success', message);
    }
</script>
@endsection