<?php

namespace App\Http\Controllers;

use App\Models\Admin\Fill;
use App\Models\Admin\Fragment;
use App\Models\Admin\Product;
use App\models\client\Custom;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CodeController extends Controller
{
    public function show($id)
    {

        $products = [];
        $customs = Custom::where('code_id', '=', $id)->get();
        $resume = [];
        

        foreach ($customs as $custom) {

            $product = Product::where('name', '=', $custom->product_name)->first();

            if (!array_key_exists($product->name, $resume)) {
                $resume[$product->name]['svgs'] = $product->svgs;
                $products[] = $product; 
            }
        }

        
        foreach ($resume as $key => $r) {
            $collectionCustom[] = Custom::where('code_id', '=', $id)->where('product_name', '=', $key)->get();

            $product = Product::where('name', '=', $key)->first();


            foreach ($collectionCustom as $customs) {

                foreach ($customs as $custom) {
                    $fragment = Fragment::find($custom->fragment_id);
                    
                    $fill = Fill::find($custom->fill_id);
                    
                    if ($key === $fragment->product->name) {
                        $resume[$key]['info'][] = 
                        [
                            'fragment' => Str::slug($fragment->product->name.'-'.$fragment->name , '-'),
                            'fill' => $fill->id,
                            'fill_name' => $fill->name
                        ];
                    }
                    
                }

            }

        }

        $resume =  json_decode(json_encode($resume));
        
      
        return view('admin.custom.show', ['customs' => $resume, 'products' => $products]);
    }
}
