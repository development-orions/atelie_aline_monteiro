<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Kit extends Model
{
    protected $fillable = ['name', 'id'];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
