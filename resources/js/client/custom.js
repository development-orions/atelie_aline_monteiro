

var slugify = require('slugify');

window.setFillOnInput = (productName, fragmentId, fillId) =>{
    const input = document.getElementById(`fragment-${fragmentId}`);
    input.value = `${productName},${fragmentId},${fillId}`;
}

window.setFillOnFragment = (fragment, id, product) =>{

    product = slugify(product);
    //alert(product);
    const _class = `pattern-${product}-${fragment}`.toLowerCase();
    const elms = document.querySelectorAll(`.${_class}`);


    elms.forEach(elm => {
        elm.style.fill = `url(#pattern-${id})`;
    });
}

window.validation = () => {
    const elms = document.querySelectorAll('.input-filled');
    alert(elms);
}





//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
        opacity: 0
    }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale(' + scale + ')',
                'position': 'absolute'
            });
            next_fs.css({
                'left': left,
                'opacity': opacity
            });
        },
        duration: 800,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
        opacity: 0
    }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'left': left
            });
            previous_fs.css({
                'transform': 'scale(' + scale + ')',
                'opacity': opacity
            });
        },
        duration: 800,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

window.onload = function() {
    const informations =JSON.parse(localStorage.getItem('atelie_informations_basic_client'));
    const babyName = informations.babyName;
    const parentName = informations.parentName;
    const parentGender = (informations.parentGender === 'mom') ? 'Mamãe':'Papai';
    const welcome  = (informations.parentGender === 'mom') ? 'vinda':'vindo';
     

    document.getElementById('parentGender').innerHTML = parentGender;
    document.getElementById('welcome').innerHTML = welcome;
    document.getElementById('parentName').innerHTML = parentName;
};

window.validation = () => {
    const form = document.getElementById('msform');
    const elms = Array.from(document.querySelectorAll('.input-filled'));
    let invalid = false;

    console.log(elms);
    elms.forEach( elm => { 
        if (elm.value === '') {
            invalid = true;
        } 
    });


    const divMessage = document.querySelector('.message');
    if (invalid) {
        divMessage.innerHTML = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>OPS!!!</strong> Aparentemente você se esqueceu de personalizar algum item. 😪
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>`;
            scrollTo({
                top: 0,
                behavior: 'smooth'
            })
    }else{
        form.submit();
    }

}


window.showModalvideo = () => {
    document.getElementById('triggerModalVideo').click();
}