<?php

namespace App\Http\Controllers;

use App\Models\Admin\Svg;
use App\Repositories\SvgRepository;
use Illuminate\Http\Request;

class SvgsController extends Controller
{

    public function __construct(SvgRepository $svgRepository)
    {
        $this->svg = $svgRepository;
    }


    public function delete($svg)
    {

        $response = ['status' => 0, 'message' => 'Falha ao deletar imagem de apresentação, tente novamente mais tarde.'];
        $svgDeleted = $this->svg->find($svg)->delete();

        if ($svgDeleted) {
            $response['status'] = 200;
            $response['message'] = 'Sucesso.';
        }
        return response()->json($response);
    }

}
