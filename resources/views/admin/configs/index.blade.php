@extends('layouts.admin.app')

@section('style')
<style>
    .dash-item {
        min-height: 90vh;
    }
</style>
@endsection

@section('content')


<div class="row">

    <div class="col-12 col-md-6 offset-md-3 p-4">

        <div class="card shadow-lg dash-item">
            <div class="card-header text-center">
                <strong>
                    <h4 class="m-0">Configurações</h4>
                </strong>
            </div>

            <div class="card-body">


                <div class="message">

                </div>

                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <strong>Informações:</strong>
                    <span>&nbsp;Você pode alterar os termos e condições para seus clientes <a href="#"
                            onclick="handleModal()">clicando
                            aqui.</a> ✅</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Atenção:</strong>
                    <span>Caso não queira modificar sua senha, <strong>basta deixar os campos de senha em
                            branco.</strong> 🔐</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form>

                    <div class="row">

                        <div class="col-12">
                            <div class="form-group m-1">
                                <label for="name" class="m-0">👼 Nome de usuário</label>
                                <input type="text" class="form-control" id="name" value="{{ $profile->name }}">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group m-1">
                                <label for="email" class="m-0">💌 E-mail de acesso</label>
                                <input type="email" class="form-control" id="email" value="{{ $profile->email }}">
                            </div>
                        </div>


                        <div class="col-12 col-md-6">
                            <div class="form-group m-1">
                                <label for="password" class="m-0">🔒 Nova senha</label>
                                <input type="password" class="form-control" id="password">
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group m-1">
                                <label for="confirmPassword" class="m-0">🔒 Confirmar nova senha</label>
                                <input type="password" class="form-control" id="confirmPassword">
                            </div>
                        </div>

                        <div class="col-12 m-1">
                            <button type="submit" class="btn btn-success btn-radius"
                                onclick="event.preventDefault(); sendRequestAccount();">Salvar
                                informações da conta</button>
                        </div>

                    </div>
                </form>


            </div>

        </div>

    </div>

</div>


<!-- Button trigger modal -->
<button type="button" class="d-none" data-toggle="modal" data-target="#modalTermsAndConditions"
    id="btnModalTermsAndConditions">
    Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="modalTermsAndConditions" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Termos e condições de uso da plataforma
                </h5>
                <button type="button" id="modalCloseTermsAndConditions" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <label for="termAndConditions" class="m-0">Apresentação ✅</label>
                <textarea class="form-control" name="termsAndConditions" id="termAndConditions" cols="30"
                    rows="5">{{ $termsAndConditions->body }}</textarea>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-radius" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success btn-radius"
                    onclick="sendRequestTermsAndConditions()">Salvar</button>
            </div>

        </div>
    </div>
</div>

@endsection


@section('script')
<script>
    
    let baseUrl = '{{ url('/') }}';

    const handleModal = () => {
            document.getElementById('btnModalTermsAndConditions').click();
        }

        const handleInformationsTermsAndConditions = () =>{
            let data = {
                'body': document.getElementById('termAndConditions').value
                };
            return data;
        }

        const handleInformationsAccount = () => {
            let data = {
                'name': document.getElementById('name').value,
                'email': document.getElementById('email').value,
                'password': document.getElementById('password').value,
                'confirmPassword': document.getElementById('confirmPassword').value,
            };
            return data;
        }

        const sendRequestAccount = () => {
            all_ajax(
                {
                    'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    'method': 'PUT',
                    "url": `${baseUrl}/admin/user/{{ auth()->user()->id }}/update`,
                    'payload': handleInformationsAccount(),
                    'callback': showMessage,
                    'args': undefined
                }
            );
        }

        const sendRequestTermsAndConditions = () => {
            all_ajax(
                {
                    'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    'method': 'PUT',
                    "url": `{{ route('admin.configs.termsAndConditions', ['id' => $termsAndConditions->id]) }}`,
                    'payload': handleInformationsTermsAndConditions(),
                    'callback': showMessage,
                    'args': undefined
                }
            );
            document.getElementById('modalCloseTermsAndConditions').click();
        }

        const showMessage = (data) => {

            if (data === 'terms_ok') {
                showToast('success', 'Os termos e condições foram atualizados com sucesso.');
            } else if(data === 'terms_fail') {
                showToast('error', 'OPS! Houve um erro ao atualizar os termos e condições.');
            }else{
                if (data === 'success') {
                    showToast('success', 'Seu perfil foi editado com sucesso!');
                } else {
                    if (data === 'password_invalid') {
                        showToast('error', 'As senhas não são iguais.');
                    }else if (data === 'email_invalid'){
                        showToast('error', 'O e-mail inserido não é válido.');
                    } 
                    else {
                        showToast('error', 'Houve um problema em editar seu perfil!');
                    }
                }
            }
        }
</script>
@endsection