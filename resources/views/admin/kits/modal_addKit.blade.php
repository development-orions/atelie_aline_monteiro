<!-- Large modal -->
<button type="button" class="btn btn-success btn-radius ml-2" data-toggle="modal" data-target=".modal">
    ADICIONAR
</button>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Você está criando um novo KIT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{ route('admin.kits.store') }}" method="POST" id="addKit">
                    @csrf
                    <div class="row">

                        <div class="col-12 form-group">
                            <label for="name" class="m-0">Escolha qual o nome do KIT</label>
                            <input type="text" name="name" id="name" class="form-control"
                                placeholder="Exemplo: Kit Caminha Dinossauro">
                        </div>
                        <div class="col-12">
                            <h5>Escolha os produtos</h5>
                            <div class="row">
                                @foreach ($products as $product)

                                    <div class="pretty p-svg p-curve m-2">
                                        <input type="checkbox" name="products[]"
                                            value="{{ $product->id }}" />
                                        <div class="state p-success">
                                            <!-- svg path -->
                                            <svg class="svg svg-icon" viewBox="0 0 20 20">
                                                <path
                                                    d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"
                                                    style="stroke: white;fill:white;"></path>
                                            </svg>
                                            <label>{{ $product->name }}</label>
                                        </div>
                                    </div>


                                @endforeach
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-radius" onclick="event.preventDefault(); document.getElementById('addKit').submit()">Salvar KIT</button>
            </div>
        </div>
    </div>
</div>
