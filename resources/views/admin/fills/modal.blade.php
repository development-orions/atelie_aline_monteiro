<div class="modal fade bd-example-modal-lg modal-fills modal-fill-{{ $fragment->id }}" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">As estampas para
                    {{ $fragment->name }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">

                    @if ($fragment->fills)
                    @foreach ($fragment->fills as $fill)
                    <div class="col-12 col-md-3 p-1" data-id="{{ $fill->id }}">
                        <i class="fa fa-times-circle btn-remove"
                            onclick="removeFill(this)"></i>

                        @if (!$fill->status)
                        <i class="fa fa-bookmark btn-enable"
                            onclick="handleStatus(this, '{{ $fill->id }}', '{{ $fill->status }}')"></i>
                        @else
                        <i class="fa fa-bookmark-o btn-disable"
                            onclick="handleStatus(this, '{{ $fill->id }}', '{{ $fill->status }}')"></i>
                        @endif



                        <img src="{{ asset('/storage/' . $fill->location) }}" width="100%">
                    </div>
                    @endforeach
                    @else

                    <div class="col-12">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Sem estampas!</strong> Parece que este fragmento não possui nenhuma estampa.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <img src="https://www.flaticon.com/svg/static/icons/svg/2742/2742329.svg" height="166px">
                    </div>

                    @endif

                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" onclick="freshFill('{{ $fragment->id }}', '{{ $fragment->name }}');">Nova
                    Estampa</a>
            </div>
        </div>
    </div>
</div>