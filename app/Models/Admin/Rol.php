<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $fillable = ['name', 'code_id'];

    public function code()
    {
        return $this->belongsTo(Code::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
