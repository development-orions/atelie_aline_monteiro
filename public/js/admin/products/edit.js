/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/admin/products/edit.js":
/*!*********************************************!*\
  !*** ./resources/js/admin/products/edit.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

window.freshFill = function (id, name) {
  document.getElementById('fill-name').innerHTML = name;
  document.getElementById('fragment_id').value = id;
  closeModalFillsAndOpenNewFill();
};

window.closeModalFillsAndOpenNewFill = function () {
  $(".modal-fills").modal('hide');
  $('body').removeClass('modal-open');
  $('.modal-backdrop').remove();
  $("#modal-new-fill").modal('show');
};

(function (document, window, index) {
  var inputs = document.querySelectorAll('.inputfile');
  Array.prototype.forEach.call(inputs, function (input) {
    var label = input.nextElementSibling,
        labelVal = label.innerHTML;
    input.addEventListener('change', function (e) {
      var fileName = '';
      if (this.files && this.files.length > 1) fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);else fileName = e.target.value.split('\\').pop();
      if (fileName) label.querySelector('span').innerHTML = fileName;else label.innerHTML = labelVal;
    }); // Firefox bug fix

    input.addEventListener('focus', function () {
      input.classList.add('has-focus');
    });
    input.addEventListener('blur', function () {
      input.classList.remove('has-focus');
    });
  });
})(document, window, 0);

window.removeApresentationPhoto = function (elm) {
  var elementId = elm.parentNode.getAttribute('data-id');
  sweetAlert_confirm('Tem certeza que deseja excluir esta imagem?', 'Assim que excluir, não será possível recuperá-la.', true, false, 'SIM', '#A20A0A', 'NÃO', '#556052', all_ajax, {
    'headers': {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    'method': 'DELETE',
    "url": "http://atelie.local/admin/svgs/delete/".concat(elementId),
    'payload': {},
    'callback': removePhotoFromScreen,
    'args': {
      'element': elm.parentNode,
      'message': '<strong>OBA!</strong> A Imagem de apresentação foi removida com sucesso.'
    }
  });
};

window.removeFill = function (elm) {
  console.log(elm.parentNode);
  var elementId = elm.parentNode.getAttribute('data-id');
  sweetAlert_confirm('Tem certeza que deseja excluir esta estampa?', 'Caso necessário, você pode cadastrar a estampa novamente.', true, false, 'SIM', '#A20A0A', 'NÃO', '#556052', all_ajax, {
    'headers': {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    'method': 'DELETE',
    "url": "http://atelie.local/admin/fills/delete/".concat(elementId),
    'payload': {},
    'callback': removePhotoFromScreen,
    'args': {
      'element': elm.parentNode,
      'message': 'Sucesso! A estampa foi removida com sucessso'
    }
  });
};

window.removePhotoFromScreen = function (args) {
  args.element.remove();
  console.log(args);
  showMessageSuccessRemovePhotoApresentation(args.message);
};

window.showMessageSuccessRemovePhotoApresentation = function (messageSuccess) {
  var message = document.getElementById('message-success');
  message.innerHTML = "<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\n                        ".concat(messageSuccess, "\n                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n                            <span aria-hidden=\"true\">&times;</span>\n                        </button>\n                        </div>");
};

/***/ }),

/***/ 1:
/*!***************************************************!*\
  !*** multi ./resources/js/admin/products/edit.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\wamp64\www\Orions\atelie_aline_monteiro\resources\js\admin\products\edit.js */"./resources/js/admin/products/edit.js");


/***/ })

/******/ });