<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Svg extends Model
{
    protected $fillable = ['name', 'location', 'product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
