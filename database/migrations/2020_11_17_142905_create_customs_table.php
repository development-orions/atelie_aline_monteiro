<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customs', function (Blueprint $table) {
            $table->bigIncrements('id');

            
            $table->unsignedBigInteger('code_id');
            $table->string('product_name');
            $table->unsignedBigInteger('fragment_id');
            $table->unsignedBigInteger('fill_id');

            $table->foreign('code_id')->references('id')->on('codes');
            $table->foreign('fragment_id')->references('id')->on('fragments');
            $table->foreign('fill_id')->references('id')->on('fills');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customs');
    }
}
