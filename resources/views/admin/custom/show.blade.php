@extends('layouts.admin.app')

@section('style')
<link rel="stylesheet" href="{{ asset('/css/admin/custom/show.css') }}">
@endsection

@section('content')


<div class="row">

    <div class="col-12 col-md-8 offset-md-2 p-4">

        <div class="card text-center shadow-lg dash-item">
            <div class="card-header">
                <strong>Resumo das personalizações</strong>
            </div>
            <div class="card-body">

                <div class="mb-3">
                    <form action="{{ route('admin.email.send') }}" method="GET">
                        <button type="submit" class="btn btn-primary btn-sm"
                            onclick="event.preventDefault(); sendForEmail(this);">Enviar para e-mail</button>
                    </form>
                </div>

                <div class="row">
                    <div class="col-12">
                        <svg class="position-absolute" style="z-index: -5">
                            <defs>
                                @foreach ($products as $product)

                                @foreach ($product->fragments as $fragment)

                                @foreach ($fragment->fills as $fill)
                                <pattern id="pattern-{{ $fill->id }}" patternUnits="userSpaceOnUse" width="400"
                                    height="400">
                                    <image src="{{ asset('/storage/' . $fill->location) }}"
                                        href="{{ asset('/storage/' . $fill->location) }}" x="0" y="0" width="400"
                                        height="400" />
                                </pattern>
                                @endforeach

                                @endforeach

                                @endforeach
                            </defs>
                        </svg>


                        <div>
                            @foreach ($customs as $key => $custom)



                            <div class="row">

                                <div class="col-12 mb-3">
                                    <h2 class="data-information-key">{{ $key }}</h2>
                                    <hr class="my-2">
                                </div>

                                <div class="col-12">
                                    @foreach ($custom->svgs as $svg)
                                    {!! file_get_contents(asset($svg->location)) !!}
                                    @endforeach
                                </div>


                                <div class="col-12 mt-3">
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">

                                        <h4 class="alert-heading m-0 font-weight-bold">Resumo</h4>
                                        @foreach ($custom->info as $inf)
                                        <p class="m-0">

                                            O fragmento <strong
                                                class="text-uppercase data-information-item-{{ Str::slug($key) }}">{{ explode('-', $inf->fragment)[count(explode('-', $inf->fragment))-1] }}</strong>
                                            possui a
                                            estampa <strong
                                                class="text-uppercase data-information-fill-{{ Str::slug($key) }}">{{ $inf->fill_name }}</strong>
                                        </p>
                                        <input type="text" class="fill d-none"
                                            value="pattern-{{ $inf->fragment }} pattern-{{ $inf->fill }}">
                                        @endforeach

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>

                            </div>

                            @endforeach
                        </div>

                    </div>
                </div>


            </div>
        </div>

    </div>

</div>



@endsection

@section('script')
<script src="{{ asset('js/html2canva.js') }}"></script>
<script src="{{ asset('js/admin/custom/show.js') }}"></script>

<script>
    let url = '{{ route('admin.email.send') }}';
    let message = 'As informações foram enviadas para contato@ateliealinemonteiro.com.br';
    function sendForEmail(elm) {
       
        
        let keys = document.querySelectorAll('.data-information-key');
        let _keys = [];
        var data = {  };

        

       keys.forEach(key => {
           let slug = string_to_slug(key.innerHTML);
           
            let fragments = document.querySelectorAll(`.data-information-item-${slug}`);
            let fills = document.querySelectorAll(`.data-information-fill-${slug}`);
            
            let _fragments = [];
            let _fills = [];

            fragments.forEach(fragment => { _fragments.push(fragment.innerHTML) });
            fills.forEach(fill => { _fills.push(fill.innerHTML) });

            
            _fragments = _fragments.join();
            _fills = _fills.join();
            
            data[key.innerHTML]= 
                {
                    "product": key.innerHTML,
                    "fragments": _fragments,
                    "fills": _fills
                };
       });

      
      console.log(data);
       
        
        
       setTimeout(() => {
        all_ajax(
        {
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            'method': 'POST',
            "url": url,
            'payload': data,
            'callback': showToast('success', 'Sucesso ao enviar para e-mail.'),
            'args': undefined
            }
        );
       }, 400);

       elm.setAttribute('disabled', 'true');
    }


    function string_to_slug (text) {
    text = text.replace(/^\s+|\s+$/g, ''); // trim
    text = text.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        text = text.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    text = text // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace('ã', 'a') // collapse whitespace and replace by -
        .replace(/[^a-z0-9 -]/g, '')
        .replace(/-+/g, '-'); // collapse dashes

    return text;
}
    
    
</script>
@endsection