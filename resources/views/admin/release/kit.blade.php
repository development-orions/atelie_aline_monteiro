 <!-- Trigger Release Kit -->
 <button type="button" class="btn btn-primary btn-radius" data-toggle="modal" data-target=".releaseKit">
     Liberar Kit
 </button>
 <!-- Modal Release Kit -->
 <div class="modal fade releaseKit" id="releaseKit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Selecione os produtos que você deseja</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

                 <form action="{{ route('admin.release.kit') }}" method="POST" id="formReleaseKit">
                     @csrf
                     @foreach ($kits as $kit)
                         <div class="pretty p-icon p-round">
                             <input type="radio" name="kit" value="{{ $kit->id }}" />
                             <div class="state p-success">
                                 <i class="icon mdi mdi-check"></i>
                                 <label>{{ $kit->name }}</label>
                             </div>
                         </div>
                     @endforeach
                 </form>


             </div>
             <div class="modal-footer">
                 <button type="submit" class="btn btn-success btn-radius"
                     onclick="event.preventDefault(); getInformations();">
                     Gerar código de acesso
                 </button>
             </div>
         </div>
     </div>
 </div>
