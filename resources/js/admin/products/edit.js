window.freshFill = (id, name) => {

    document.getElementById('fill-name').innerHTML = name;
    document.getElementById('fragment_id').value = id;

    closeModalFillsAndOpenNewFill();
}


window.closeModalFillsAndOpenNewFill = () => {
    $(".modal-fills").modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    $("#modal-new-fill").modal('show');
}


;
(function(document, window, index) {
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function(input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function(e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}',
                    this.files.length);
            else
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener('focus', function() {
            input.classList.add('has-focus');
        });
        input.addEventListener('blur', function() {
            input.classList.remove('has-focus');
        });
    });
}(document, window, 0));



window.removeApresentationPhoto = (elm) => {
    let elementId = elm.parentNode.getAttribute('data-id');
    sweetAlert_confirm('Tem certeza que deseja excluir esta imagem?',
        'Assim que excluir, não será possível recuperá-la.',
        true,
        false,
        'SIM',
        '#A20A0A',
        'NÃO',
        '#556052',
        all_ajax, {
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            'method': 'DELETE',
            "url": `http://atelie.local/admin/svgs/delete/${elementId}`,
            'payload': {},
            'callback': removePhotoFromScreen,
            'args': {
                'element': elm.parentNode,
                'message': '<strong>OBA!</strong> A Imagem de apresentação foi removida com sucesso.'
            }
        }
    );
}


window.removeFill = (elm) => {
    console.log(elm.parentNode);
    let elementId = elm.parentNode.getAttribute('data-id');
    sweetAlert_confirm('Tem certeza que deseja excluir esta estampa?',
        'Caso necessário, você pode cadastrar a estampa novamente.',
        true,
        false,
        'SIM',
        '#A20A0A',
        'NÃO',
        '#556052',
        all_ajax, {
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            'method': 'DELETE',
            "url": `http://atelie.local/admin/fills/delete/${elementId}`,
            'payload': {},
            'callback': removePhotoFromScreen,
            'args': {
                'element': elm.parentNode,
                'message': 'Sucesso! A estampa foi removida com sucessso'
            }
        }
    );
}








window.removePhotoFromScreen = (args) => {
    args.element.remove();
    console.log(args);
    showMessageSuccessRemovePhotoApresentation(args.message);
}

window.showMessageSuccessRemovePhotoApresentation = (messageSuccess) => {
    let message = document.getElementById('message-success');
    message.innerHTML = `<div class="alert alert-success alert-dismissible fade show" role="alert">
                        ${messageSuccess}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>`;
}
