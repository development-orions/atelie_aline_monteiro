 <!-- Trigger Release Product -->
 <button type="button" class="btn btn-primary btn-radius" data-toggle="modal" data-target=".modal">
    Liberar Produtos
</button>
<!-- Modal Release Products -->
<div class="modal fade" id="releaseProduct" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Selecione os produtos que você deseja</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                @foreach ($products as $product)
                    <div class="pretty p-svg p-curve">
                        <input type="checkbox" name="products[]" value="{{ $product->id }}" />
                        <div class="state p-success">
                            <!-- svg path -->
                            <svg class="svg svg-icon" viewBox="0 0 20 20">
                                <path
                                    d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"
                                    style="stroke: white;fill:white;"></path>
                            </svg>
                            <label>{{ $product->name }}</label>
                        </div>
                    </div>
                @endforeach


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-radius"
                    onclick="event.preventDefault(); getInformations();">
                    Gerar código de acesso
                </button>
            </div>
        </div>
    </div>
</div>
