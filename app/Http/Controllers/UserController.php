<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $user = $this->user->find(auth()->user()->id);

        $isEmail = filter_var( $data['email'], FILTER_VALIDATE_EMAIL );
        
        if ($isEmail) {
            if (!$data['password']) {
                $user->name = $data['name'];
                $user->email = $data['email'];
            } else {
    
                if ($data['password'] !== $data['confirmPassword']) {
                    return 'password_invalid';
                } else {
                    $user->name = $data['name'];
                    $user->email = $data['email'];
                    $user->password = bcrypt($data['password']);
                }
            }
            return $user->save() ? 'success' : 'error';
        } else {
            return 'email_invalid';
        }  
    }

}
