<?php

namespace App\Repositories;

use App\Models\Admin\Code;
use App\Repositories\BaseRepository;

/**
 * Class CodeRepository
 * @package App\Repositories
 * @version November 12, 2020, 5:36 pm UTC
*/

class CodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Code::class;
    }
}
