@extends('layouts.client.app')


@section('style')
<link rel="stylesheet" href="{{ asset('css/client/home.css') }}">
@endsection

@section('content')



<div class="align">
    <div class="grid">

        <form class="form">

            <!-- message for flash or setting manual -->
            <div class="p-3" id="message">
                @include('flash::message')
            </div>

            <!-- input parent name -->
            <div class="form__field">
                <span class="icon">
                    <img width="38px" src="https://www.flaticon.com/svg/static/icons/svg/2439/2439106.svg">
                </span>
                <input id="parentName" type="text" name="parentName" class="form__input" placeholder="Qual seu nome?"
                    required>
            </div>


            <!-- input baby name -->
            <div class="form__field">
                <span class="icon">
                    <img width="38px" src="https://www.flaticon.com/svg/static/icons/svg/3636/3636153.svg">
                </span>
                <input id="babyName" type="text" name="babyName" class="form__input"
                    placeholder="Qual o nome do seu bebê?" required>
            </div>


            <!-- handle parent gender -->
            <div class="row m-0">
                <!-- input parent gender -->
                <div class="col-12">
                    <input type="text" class="d-none" id="parentGender" value="">
                </div>

                <!-- buttons for select gender -->
                <div class="col-12 col-md-6">
                    <button class="btn btn-secondary btn-block btn-parent"
                        onclick="event.preventDefault(); handleParentGender(this, 'mom');">
                        <strong>MAMÃE</strong>
                    </button>
                </div>
                <div class="col-12 col-md-6">
                    <button class="btn btn-secondary btn-block btn-parent"
                        onclick="event.preventDefault(); handleParentGender(this, 'father')">
                        <strong>PAPAI</strong>
                    </button>
                </div>

            </div>


            <div class="row m-0 mt-3">
                <div class="col-12">
                    <span>
                        <a href="#" onclick="event.preventDefault(); handleModalTermsAndConditions()">veja os termos e
                            condições</a>
                    </span>
                </div>
            </div>

            <div class="row m-0">
                <div class="col-12">

                    <label class="w-100">
                        <input type="checkbox" name="checkTerms" id="checkTerms" style="height: 13px !important">
                        <span class="ml-2">
                            Eu aceito os termos e condições.
                        </span>
                    </label>
                </div>
            </div>


            <!-- fake submit -->
            <div class="form__field">
                <input type="submit" value="Prosseguir" onclick="event.preventDefault(); handleValidations();">
            </div>

        </form>

        <!-- helper for client -->
        <div class="row">
            <div class="col-12 text-center">
                <p class="m-0 mt-4">Que tal uma ajuda antes de personalizar?</p>
                <a href="#" onclick="event.preventDefault(); showModalvideo()">Assistir vídeo</a>
            </div>
        </div>

    </div>
</div>

<!-- Button trigger modal -->
<button type="button" id="modal-terms-and-conditions" class="d-none" data-toggle="modal" data-target="#modal-terms">
    termos e condições
</button>

<!-- Modal -->
<div class="modal fade" id="modal-terms" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
    style="color: #2c3338;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Termos e condições</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               {{ $termsAndConditions->body }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">
                    Fechar
                </button>
            </div>
        </div>
    </div>
</div>


<!-- Button trigger modal -->
<button type="button" class="d-none" id="triggerModalVideo" data-toggle="modal" data-target="#modalVideo">
    Trigger Modal Video
</button>

<!-- Modal -->
<div class="modal fade" id="modalVideo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #2c3338">Aprendendo a personalizar itens na plataforma 😜</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <video controls="true" src="{{ asset('video/teste.mp4') }}" width="100%"></video>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Já assisti o vídeo</button>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script src="{{ asset('/js/client/home.js') }}"></script>

<!--
    *   the script below is here because 
    *   it has snippets of laravel 
    -->
<script>
    let baseUrl = '{{ url('/') }}';
    
    window.handleValidations = () => {
        let informations = handleInformations();
        if (informations.babyName === '' || informations.parentName === '' || informations.parentGender === '') {
            document.getElementById('message').innerHTML = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <strong>OPS!!!</strong> Ainda falta algumas informações para que possamos continuar, 
                                    caso precise de ajuda, você pode <strong>assistir este <a href='#'>vídeo.</a><strong>  😜
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>`;
        } else {
            if (!handleTermsAndConditions()) {
                document.getElementById('message').innerHTML = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <strong>OPS!!!</strong> 
                                    
                                    Parece que você esqueceu de <strong>aceitar os termos e condições</strong> da nossa plataforma,
                                     você pode vê-las <a href='#' onclick='handleModalTermsAndConditions()'>clicando aqui</a> 😊✨

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>`;
            } else {
                let code = "{{ Request::route('code') }}";
                informations = JSON.stringify(informations); //convert object to json (in text);
                localStorage.setItem('atelie_informations_basic_client', informations); //set "json" (in text) on localstorage
                window.location.href = `${baseUrl}/client/custom/${code}`;   
            }
        }
    }
</script>
@endsection