<?php

namespace App\Models\Admin;

use App\models\client\Custom;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $fillable = [
        'code'
    ];

    public function rol()
    {
        return $this->hasOne(Rol::class);
    }

    public function customs()
    {
        return $this->hasMany(Custom::class);
    }
}
