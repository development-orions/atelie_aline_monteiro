window.addProductToKit = (elm) =>{
    let checkBox = elm.children[0].children[0];
    checkBox.checked = !checkBox.checked;
    if (checkBox.checked) {
        elm.children[0].classList.add('itemChecked');
    } else {
        elm.children[0].classList.remove('itemChecked');
    }
}
