<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Admin\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    public function index()
    {
        $products = $this->productRepository->all();
        $products = $products->count() === 0 ? null : $products;
        return view('admin.products.index', ['products' => $products]);
    }


    public function edit($product)
    {

        $product = $this->productRepository->find($product);
        if (!$product) {
            return redirect()->route('admin.products.index');
        }


        $fragments = [];
        foreach ($product->fragments as $frag) {
            $fragments[] = [
                'id' => $frag->id,
                'name' => $frag->name,
                'fills' => $frag->fills
            ];
        }

        $svgs = $product->svgs;


        return view('admin.products.edit', [
            'product' => $product,
            'fragments' => json_decode(json_encode($fragments)),
            'svgs' => $svgs
            ]);
    }

    public function update(ProductRequest $productRequest, $product)
    {
        $input = $productRequest->all();

        $product = $this->productRepository->update($input, $product);

        flash('O produto foi atualizado com sucesso!')->success()->important();
        return redirect()->route('admin.products.index');
    }
}
