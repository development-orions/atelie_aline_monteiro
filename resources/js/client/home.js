
window.handleParentGender = (elm, parentGender) => {
    const btns = document.querySelectorAll('.btn-parent');
    btns.forEach(element => {
        element.classList.remove('btn-success');
        element.classList.add('btn-secondary')
    });
    elm.classList.remove('btn-secondary');
    elm.classList.add('btn-success');
    document.getElementById('parentGender').value = parentGender;
}

window.handleInformations = () => {
    const informations = {
        'babyName': document.getElementById('babyName').value,
        'parentGender': document.getElementById('parentGender').value,
        'parentName': document.getElementById('parentName').value
    }

    return informations;
}


window.handleModalTermsAndConditions = () => {
    document.getElementById('modal-terms-and-conditions').click();
}
window.handleTermsAndConditions = () => {
    return document.getElementById('checkTerms').checked;
}

window.showModalvideo = () => {
    document.getElementById('triggerModalVideo').click();
}
