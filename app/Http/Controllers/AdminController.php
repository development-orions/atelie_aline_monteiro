<?php

namespace App\Http\Controllers;

use App\Models\Admin\Code;
use App\Repositories\CodeRepository;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct(CodeRepository $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }


    public function index()
    {
        $codes = Code::orderBy('id', 'DESC')->paginate(10);
        return view('admin.home', ['codes' => $codes]);
    }
}
