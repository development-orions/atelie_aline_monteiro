<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\SendProductsInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;

Auth::routes(['register' => false]);


Route::get('/', 'HomeController@index');


Route::group(['middleware' => ['auth']], function(){

    Route::prefix('admin')->name('admin.')->group(function () {

        Route::get('/', 'AdminController@index')->name('home');

        Route::post('/send-email', function (Request $request) {
            $data = $request->all();
            Mail::to(env('APP_EMAIL'))->send(new SendProductsInformation($data));
        })->name('email.send');

        Route::prefix('products')->name('products.')->group(function () {
            Route::get('/', 'ProductsController@index')->name('index');
            Route::get('/{product}/edit', 'ProductsController@edit')->name('edit');
            Route::put('update/{product}', 'ProductsController@update')->name('update');
        });

        Route::prefix('kits')->name('kits.')->group(function () {
            Route::get('/', 'KitsController@index')->name('index');
            Route::get('/{kit}/edit', 'KitsController@edit')->name('edit');
            Route::put('/update/{kit}', 'KitsController@update')->name('update');
            Route::delete('/delete/{kit}', 'KitsController@destroy')->name('destroy');
            Route::post('/store', 'KitsController@store')->name('store');
        });

        Route::prefix('fills')->name('fills.')->group(function () {
            Route::post('/store', 'FillsController@store')->name('store');
            Route::delete('/delete/{fill}', 'FillsController@destroy')->name('destroy');
            Route::get('/{fill}/update', 'FillsController@update')->name('update');
        });


        Route::prefix('svgs')->name('svgs.')->group(function () {
            Route::delete('/delete/{svg}', 'SvgsController@delete')->name('delete');
        });

        Route::prefix('release')->name('release.')->group(function() {
            Route::post('/products', 'ReleaseController@product')->name('product');
            Route::post('/kits', 'ReleaseController@kit')->name('kit');
        });

        Route::prefix('configs')->name('configs.')->group(function() {
            Route::get('/', 'ConfigController@index')->name('index');
            Route::put('/{id}/termsAndConditions', 'ConfigController@updateTermsAndConditions')->name('termsAndConditions');
        });

        Route::prefix('user')->name('user.')->group(function() {
            Route::put('/{id}/update', 'UserController@update')->name('update');
        });

        Route::prefix('codes')->name('codes.')->group(function() {
            Route::get('/{code}', 'CodeController@show')->name('show');
        });


    });
});

Route::prefix('client')->name('client.')->group(function () {
    Route::get('/{code}', 'ClientController@home')->name('home');

    Route::prefix('custom')->name('custom.')->group(function() {
        Route::get('/{code}', 'ClientController@index')->name('index');
        Route::post('/effect', 'ClientController@effect')->name('effect');
    });
});




Route::prefix('artisan')->name('artisan.')->group(function() {
    Route::get('/storage-link', function () {
        \Artisan::call('storage:link');
    });
});


