<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description',];

    public function rols()
    {
        return $this->belongsToMany(Rol::class);
    }

    public function svgs()
    {
        return $this->hasMany(Svg::class);
    }

    public function fragments()
    {
        return $this->hasMany(Fragment::class);
    }

    public function kits()
    {
        return $this->belongsToMany(Kit::class);
    }
}
