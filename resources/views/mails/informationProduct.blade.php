<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@900&display=swap" rel="stylesheet">


    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: 'Noto Sans JP', sans-serif;
        }


        div.header {
            max-width: 100%;
            width: 100% !important;
            padding: 2%;
            text-align: center;
            background: #eee;
        }

        div.content {
            padding: 2%;
            background: #eee;
        }
    </style>
</head>

<body>

    <div class="header">
        <h1>Sistema | Ateliê Aline Monteiro</h1>
    </div>

    <div class="content">
        <h4>Produtos Personalizados</h4>

        @foreach ($data as $d)
        <h3 style="background: #404040; color: #FFF; margin-inline: 8px; padding: 8px; !important;">{{ $d['product'] }}</h3>


        @for ($i = 0; $i < count(explode(',', $d['fragments'])); $i++) 
        <div style="margin-top: 20px">
            <span
                style="background: coral; color: #FFF; margin-inline: 8px; padding: 8px; !important;">{{ explode(',', $d['fragments'])[$i] }}</span>
            <span
                style="background: coral; color: #FFF; margin-inline: 8px; padding: 8px; !important;">{{ explode(',', $d['fills'])[$i] }}</span>
        </div>
        <br>
        @endfor


        @endforeach
    </div>




</body>

</html>