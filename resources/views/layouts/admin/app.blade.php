<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ateliê Aline Monteiro') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">

    <!-- SweetAlert 2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">


    @yield('style')
</head>

<body class="sidebar-is-reduced">

    @php
    $navs = [
    [
    'icon' => 'fas fa-tachometer-alt',
    'text' => 'Pagina Inicial',
    'href' => route('admin.home')
    ],
    [
    'icon' => 'fa fa-th-large',
    'text' => 'Produtos',
    'href' => route('admin.products.index')
    ],
    [
    'icon' => 'fa fa-th-list',
    'text' => 'Kits',
    'href' => route('admin.kits.index')
    ],
    [
    'icon' => 'fas fa-cogs',
    'text' => 'Configurações',
    'href' => route('admin.configs.index')
    ],
    ];


    @endphp

    <header class="l-header">
        <div class="l-header__inner clearfix">
            <div class="c-header-icon js-hamburger">
                <div class="hamburger-toggle"><span class="bar-top"></span><span class="bar-mid"></span><span
                        class="bar-bot"></span></div>
            </div>


            <div class="header-icons-group" onclick="event.preventDefault(); document.getElementById('form-logout').submit()">
                <div class="c-header-icon logout"><i class="fa fa-power-off"></i></div>

                <form action="{{ route('logout') }}" method="POST" id="form-logout">@csrf</form>
                
            </div>

        </div>
    </header>

    <div class="l-sidebar">
        <div class="logo">
            <div class="logo__txt">AM</div>
        </div>
        <div class="l-sidebar__content">

            <nav class="c-menu js-menu">
                <ul class="u-list">

                    @foreach ($navs as $nav)
                    <li class="c-menu__item is-active" data-toggle="tooltip" title="{{ $nav['text'] }}">
                        <a href="{{ $nav['href'] }}">
                            <div class="c-menu__item__inner"><i class="{{ $nav['icon'] }}"></i>
                                <div class="c-menu-item__title"><span>{{ $nav['text'] }}</span></div>
                            </div>
                        </a>
                    </li>
                    @endforeach

                </ul>
            </nav>

        </div>
    </div>

    <main class="l-main">
        <div class="content-wrapper content-wrapper--with-bg area">
            @include('flash::message')
            @yield('content')
        </div>
    </main>





    <script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        let Dashboard = (() => {
    let global = {
      tooltipOptions: {
        placement: "right" },
  
      menuClass: ".c-menu" };
  
  
    let menuChangeActive = el => {
      let hasSubmenu = $(el).hasClass("has-submenu");
      $(global.menuClass + " .is-active").removeClass("is-active");
      $(el).addClass("is-active");
    };
  
    let sidebarChangeWidth = () => {
      let $menuItemsTitle = $("li .menu-item__title");
      let logo__txt = document.querySelector('.logo__txt');
      
      logo__txt.innerHTML = (logo__txt.innerHTML === 'AM') ? 'Aline Monteiro' : 'AM';

      
      $("body").toggleClass("sidebar-is-reduced sidebar-is-expanded");
      $(".hamburger-toggle").toggleClass("is-opened");
  
      
  
    };
  
    return {
      init: () => {
        $(".js-hamburger").on("click", sidebarChangeWidth);
  
        $(".js-menu li").on("click", e => {
          menuChangeActive(e.currentTarget);
        });
  
        $('[data-toggle="tooltip"]').tooltip(global.tooltipOptions);
      } };
  
  })();
  
  Dashboard.init();
    </script>
    @yield('script')

</body>




</body>

</html>