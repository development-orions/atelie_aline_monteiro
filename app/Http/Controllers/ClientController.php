<?php

namespace App\Http\Controllers;

use App\Models\Admin\Code;
use App\models\client\Custom;
use App\TermsAndConditions;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    public function __construct(Code $code, TermsAndConditions $termsAndConditions)
    {
        $this->code = $code;
        $this->termsAndConditions = $termsAndConditions;
    }


    public function home()
    {
        $termsAndConditions = $this->termsAndConditions->find(1);
        return view('client.home', ['termsAndConditions' => $termsAndConditions]);
    }

    public function index($code)
    {

        $codeFound = $this->code->where('code', $code)->first();


        if ($codeFound) {
            if ($codeFound->deleted_at) {
                flash('Ops! <strong>O código fornecido já foi utilizado.</strong> Caso você ainda não tenha realizado sua personalização, solicite um novo código ao Ateliê. 😉')->error()->important();
            } else {
                $products = $codeFound->rol->products;
                return view('client.custom', ['products' => $products, 'code' => $codeFound->id]);
            }
        } else {
            flash('Ops! O código fornecido não é válido. Por favor, solicite outro ao Ateliê.')->error()->important();
        }


        return redirect()->route('client.home', ['code' => $code]);
    }


    public function effect(Request $request)
    {
        unset($request['_token']);
        $code = $request['code'];
        unset($request['code']);

        $codeFound = Custom::where('code_id', $code)->first();

        foreach ($request->all() as $customs) {

            foreach ($customs as $custom) {
                $custom = explode(',', $custom);

               
                $result = null;

                $codeOriginal = $this->code::find($code);

                if (!$codeFound) {
                    $result = Custom::insert([
                        'code_id' => $code,
                        'product_name' => $custom[0],
                        'fragment_id' => $custom[1],
                        'fill_id' => $custom[2],
                    ]);
                }
            }

        }
        
        if ($result) {
            flash('<strong>Muito obrigado!</strong> Mais um passo para a construção do seu sonho foi <strong>realizado com sucesso!!!</strong> Em breve o seu pedido já estará sendo confeccionado 🤩')->success()->important();
        } else {
            flash('Ops! Aconteceu algum imprevisto e não foi possível salvar suas alterações, se o erro persistir, entre em contato com o Ateliê.')->error()->important();
        }

        $codeOriginal->deleted_at = now();
        $codeOriginal->save();
        return redirect()->route('client.home', ['code' => $codeOriginal->code]);
    }
}
