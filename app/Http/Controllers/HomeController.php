<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /* Constructor */
    public function __construct()
    {

    }


    /* Index */
    public function index()
    {
        return redirect()->route('login');
    }



}
