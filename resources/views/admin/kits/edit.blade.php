@extends('layouts.admin.app')

@section('style')
    <style>
        .product-item {
            background: #f4f4f2;
            padding: 8px;
            min-height: 120px;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
        }

        .itemChecked{
            border: 2px solid #a8dda8;
        }

    </style>
@endsection


@section('content')




    <div class="row px-2 px-md-0">


        <div class="col-12 col-md-6 offset-md-3 mt-4">
            @include('flash::message')
        </div>

        <div class="col-12 col-md-6 offset-md-3">
            <h2 class="my-4">{{ $kit->name }}</h2>

        <form action="{{route('admin.kits.update', ['kit' => $kit->id])}}" method="POST">

                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="kitName" class="m-0">Nome do Kit</label>
                    <input type="text" id="kitName" name="name" class="form-control" value="{{ $kit->name }}">
                </div>


                <div class="row">
                    <div class="col-12">
                        <h2 class="m-0">Produtos</h2>
                        <span>Selecione os produtos que irão compor o KIT</span>
                    </div>
                </div>

                <div class="row">
                    @foreach ($products as $product)


                        <div class="col-12 col-md-3 p-1 text-center" onclick="addProductToKit(this)">

                            <div class="product-item @if($kit->products->contains($product)) itemChecked @endif">
                                <input type="checkbox" class="d-none" name="products[]" value="{{$product->id}}"
                                    @if ($kit->products->contains($product))
                                        checked
                                    @endif
                                >
                                <label class="form-check-label" for="exampleCheck1">{{$product->name}}</label>
                            </div>
                        </div>


                    @endforeach
                </div>


                <div class="row">
                    <div class="col-12 mt-2 mb-5">
                        <button class="btn btn-success mr-1 float-right">Salvar Alterações</button>
                    </div>
                </div>


            </form>

        </div>

    </div>




@endsection


@section('script')
<script src="{{asset('js/admin/kits/edit.js')}}"></script>
@endsection
