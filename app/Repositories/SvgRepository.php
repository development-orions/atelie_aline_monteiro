<?php

namespace App\Repositories;

use App\Models\Admin\Svg;
use App\Repositories\BaseRepository;

/**
 * Class SvgRepository
 * @package App\Repositories
 * @version November 12, 2020, 5:30 pm UTC
*/

class SvgRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'location',
        'product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Svg::class;
    }
}
