<?php

namespace App\models\client;

use App\Models\Admin\Code;
use Illuminate\Database\Eloquent\Model;

class Custom extends Model
{
    protected $fillable = ['product_name', 'fragment_id', 'fill_id'];

    public function code()
    {
        return $this->belongsTo(Code::class);
    }
}
