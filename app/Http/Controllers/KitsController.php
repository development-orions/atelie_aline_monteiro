<?php

namespace App\Http\Controllers;

use App\Models\Admin\Kit;
use App\Models\Admin\Product;
use App\Repositories\KitRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KitsController extends Controller
{

    public function __construct(KitRepository $kitRepository, ProductRepository $productRepository)
    {
        $this->kit = $kitRepository;
        $this->product = $productRepository;
    }

    public function index()
    {
        $kits = $this->kit->paginate(5);
        $products = $this->product->all();

        return view('admin.kits.index', ['kits' => $kits, 'products' => $products]);
    }


    public function edit($kit)
    {

        $kit = $this->kit->find($kit);
        $products = $this->product->all();


        return view('admin.kits.edit', ['kit' => $kit, 'products' => $products]);
    }


    public function update(Request $request, $kit)
    {
        $data = $request->all();
        $kit = $this->kit->find($kit);
        $kit->update($data);

        if (array_key_exists('products', $data)) {
            $kit->products()->sync($data['products']);
        }else{
            $kit->products()->sync([]);
        }


        flash('<strong>Sucesso!</strong> O kit foi alterado e as modicações já podem ser vistas.')->success()->important();
        return redirect()->route('admin.kits.edit', ['kit' => $kit]);
    }

    public function destroy($kit)
    {
        $kit = $this->kit->find($kit);

        if ($kit) {
            $wasDeleted = $kit->delete();
            $wasDeleted ?
                flash('O KIT FOI DELETADO <strong>COM SUCESSO!</strong>')->success()->important() :
                flash('HOUVE UM ERRO INESPERADO EM DELETAR O KIT!')->success()->important();
        } else {
            flash('NENHUM KIT COM O ID APRESENTADO FOI ENCONTRADO.')->info()->important();
        }

        return redirect()->route('admin.kits.index');
    }

    public function store(Request $request)
    {

        $data = $request->all();

        $resultTransaction = DB::transaction(function () use($data) {
            $kit = $this->kit->create($data);
            $kit->products()->sync($data['products']);

            return true;
        });

        if ($resultTransaction) {
            flash('Sucesso ao criar KIT, agora você pode liberar para seus clientes.')->success()->important();
        } else {
            flash('Erro ao criar KIT, tente novamente em alguns instantes.')->danger()->important();
        }

        return redirect()->route('admin.kits.index');

    }
}
