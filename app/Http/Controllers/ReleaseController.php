<?php

namespace App\Http\Controllers;

use App\Models\Admin\Code;
use App\Models\Admin\Product;
use App\Models\Admin\Rol;
use App\Repositories\CodeRepository;
use App\Repositories\KitRepository;
use App\Repositories\ProductRepository;
use App\Repositories\RolRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReleaseController extends Controller
{

    public function __construct(CodeRepository $codeRepository, RolRepository $rolRepository, ProductRepository $productRepository, KitRepository $kitRepository)
    {
        $this->code = $codeRepository;
        $this->rol = $rolRepository;
        $this->product = $productRepository;
        $this->kit = $kitRepository;
    }

    public function product(Request $request)
    {
        $data = $request->all();
        $data['code'] = md5(uniqid(rand(), true));
        $response = [];

        $trx = DB::transaction(function () use($data) {
            $code = $this->code->create(['code' => $data['code']]);
            $rol = $this->rol->create(['name' => $code->code, 'code_id' => $code->id]);
            $rol->products()->sync($data['products']);

            return true;
        });

        if ($trx) {
            $response['status'] = 200;
            $response['code'] = $data['code'];
            $response['message'] = 'Sucesso ao criar a lista, agora você pode enviar para seu cliente.';
        } else {
            $response['status'] = 0;
            $response['message'] = 'Houve um problema em gerar a lista, por favor, tente novamente.';
        }

        return response()->json($response);

    }

    public function kit(Request $request)
    {


        $data = $request->all();
        $response = ['status' => 0, 'message' => 'Houve um erro ao liberar o KIT.'];

        if (array_key_exists('kit', $data)) {
            $ids = [];
            $kit = $this->kit->find($data['kit']);
            foreach ($kit->products as $product) {
                $ids[] = $product->id;
            }

            $trx = DB::transaction(function () use($ids, $kit) {
                $code = $this->code->create(['code' => md5(uniqid(rand(), true))]);
                $rol = $code->rol()->create(['name' => $code->code]);
                $rol->products()->sync($ids);

                return $code->code;
            });


            if ($trx) {
                $response['status'] = 200;
                $response['message'] = "Ótimo! Você acabou de liberar o kit, agora <a href='#' onclick='copyLink()'>copie</a> o link e envie para o cliente.
                <br> link ->";
                $response['code'] = $trx;
            }


        }


        return response()->json($response);


    }

}
