<?php

use Illuminate\Database\Seeder;

class SvgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('svgs')->insert(
            [
                'name' => Str::random(10),
                'location' => 'svgs/travesseiro.svg',
                'product_id' => 1,
            ],
            [
                'name' => Str::random(10),
                'location' => 'svgs/trocador.svg',
                'product_id' => 1,
            ]
        );
    }
}
