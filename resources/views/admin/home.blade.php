@extends('layouts.admin.app')

@section('style')
    <style>
        .dash-item {
            min-height: 90vh;
        }

    </style>
@endsection

@section('content')


    <div class="row">

        <div class="col-12 col-md-8 offset-md-2 p-4">

            <div class="card text-center shadow-lg dash-item">
                <div class="card-header">
                    <strong>Últimas Liberações</strong>
                </div>
                <div class="card-body">

                    <div class="row d-none d-md-flex">
                        <div class="col-md-6 mb-3">
                            Código
                        </div>
                        <div class="col-md-3 mb-3">
                            Situação
                        </div>
                        <div class="col-md-3 mb-3">
                            Personalização
                        </div>
                    </div>

                    @foreach ($codes as $code)
                        <div class="row">
                            <div class="col-12 col-md-6 mt-2">
                                {{ $code->code }}
                            </div>
                            <div class="col-12 col-md-3 mt-2">
                                @if ($code->deleted_at)
                                    <span class="badge badge-success p-2 w-50">Concluído</span>
                                @else
                                    <span class="badge badge-warning p-2 w-50">Em progresso</span>
                                @endif
                            </div>
                            <div class="col-12 col-md-3 mt-2">
                                @if ($code->deleted_at)
                                    <a href="{{ route('admin.codes.show', ['code' => $code->id]) }}">
                                        <span class="badge badge-success p-2 w-50">Visualizar</span>
                                    </a>
                                @else
                                    <span class="badge badge-secondary p-2 w-50" aria-disabled="true">Indisponível</span>
                                @endif
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="card-footer text-muted">
                    {{ $codes->links() }}
                </div>
            </div>

        </div>

    </div>

@endsection
