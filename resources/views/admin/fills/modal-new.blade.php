<div class="modal fade bd-example-modal-lg" id="modal-new-fill" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Você está adicionando uma nova estampa em
                    <strong><span id="fill-name"></span></strong>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="form-new-fill" method="POST" action="{{ route('admin.fills.store') }}" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden"  name="product-id" value="{{ $product->id }}">

                    <!--<div>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            Você pode adicionar mais de uma estampa ao mesmo tempo, <strong>mas os nomes
                                serão os mesmos para todas.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>-->


                    <input type="hidden" name="fragment_id" id="fragment_id">

                    <div class="form-group">
                        <label for="fill-name">Nome da estampa</label>
                        <input type="text" class="form-control" name="fill-name">
                        <small id="emailHelp" class="form-text text-muted">Este nome não irá aparecer para
                            os clientes.</small>
                    </div>

                    <div class="box">
                        <input type="file" name="files[]" id="file-1" class="inputfile inputfile-1 d-none"
                            data-multiple-caption="{count} arquivo(s) selecionado(s)" />
                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                viewBox="0 0 20 17">
                                <path
                                    d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" />
                            </svg>
                            <span>Escolha a estampa</span>
                        </label>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success" onclick="event.preventDefault(); document.getElementById('form-new-fill').submit()">Cadastrar</button>
            </div>
        </div>
    </div>
</div>
