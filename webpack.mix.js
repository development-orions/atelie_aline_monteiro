const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/js')


    .js('resources/js/admin/products/edit.js', 'public/js/admin/products')
    .js('resources/js/admin/products/index.js', 'public/js/admin/products')
    .js('resources/js/admin/kits/edit.js', 'public/js/admin/kits')
    .js('resources/js/admin/custom/show.js', 'public/js/admin/custom/')


    .sass('resources/sass/app.scss', 'public/css')

    /* HOME */
    .sass('resources/sass/home/login.scss', 'public/css')

    /* ADMIN | LAYOUT */
    .sass('resources/sass/admin/app.scss', 'public/css/admin')

    /* ADMIN | PRODUCTS */
    .sass('resources/sass/admin/products/edit.scss', 'public/css/admin/products')
    .sass('resources/sass/admin/products/index.scss', 'public/css/admin/products')

    /* ADMIN | KITS */
    .sass('resources/sass/admin/kits/index.scss', 'public/css/admin/kits')

    /* ADMIN | Custom */
    .sass('resources/sass/admin/custom/show.scss', 'public/css/admin/custom/')

    /* CLIENT */

    .sass('resources/sass/client/home.scss', 'public/css/client/')
    .sass('resources/sass/client/custom.scss', 'public/css/client/')

    .js('resources/js/client/home.js', 'public/js/client/')
    .js('resources/js/client/custom.js', 'public/js/client/');
